lth1 = 0.1;

Point(1) = {-0.5, -0.5, 0.0, lth1};
Point(2) = {0.5, -0.5, 0.0, lth1};
Point(3) = {0.5, 0.5, 0.0, lth1};
Point(4) = {-0.5, 0.5, 0.0, lth1};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(1) = {1, 2, 3, 4};

Plane Surface(1) = {1};

Physical Surface(1) = {1};
Physical Line(2) = {1};
Physical Line(3) = {2};
Physical Line(4) = {3};
Physical Line(5) = {4};
