lth1 = 0.3;
lth2 = 0.2;

Point(1) = {-0.5, -0.5, 0.0, lth1};
Point(2) = {0.5, -0.5, 0.0, lth1};
Point(3) = {0.5, 0.5, 0.0, lth1};
Point(4) = {-0.5, 0.5, 0.0, lth1};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(1) = {1, 2, 3, 4};

Point(5) = {0.0, 0.0, 0.0, 1.0};
Point(6) = {2.0, 0.0, 0.0, lth2};
Point(7) = {-2.0,0.0, 0.0, lth2};

Circle(5) = {6, 5, 7};
Circle(6) = {7, 5, 6};
Line Loop(2) = {5, 6};

Plane Surface(1) = {2, 1};

Mesh.Smoothing = 100;

Physical Surface(1) = {1};
Physical Line(2) = {1, 2, 3, 4};
Physical Line(3) = {5, 6};
